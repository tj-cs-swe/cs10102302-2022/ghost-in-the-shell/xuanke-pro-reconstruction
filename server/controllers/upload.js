const uploadController = {
  upload_image(req,res){
    let file = req.file;
    res.json({
      errno: 0,
      data: {
        url:'http://localhost:3000/images/'+file.filename,
      },
    });
  },
};
module.exports = uploadController;