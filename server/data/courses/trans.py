# xls表格转json，仅支持从1.tongji导出的数据
import xlrd
import json

# 文件路径
file = '1.xlsx'
output = 'courses.json'
if __name__ == "__main__":
    table = xlrd.open_workbook(filename=file)
    sheet1 = table.sheet_by_index(0)
    # 数据库字段与excel表列的对应关系
    tags = {'_id':0,'name':1,'teacher':4,'tid':5,'place':9,'type':7,'language':8}
    
    # department字段需要特判
    department=''
    
    f = open (output, 'w', encoding='utf-8')
    for i in range(sheet1.nrows-1):
        if i <= 2:
            continue
        # 每行对应一次结果输出
        row = sheet1.row_values(i)
        result = {}
        if(row[0]==''):
            continue
        elif(row[1]==''):
            department = row[0]
            continue
        else:
            for j in tags:
                result[j] = row[tags[j]]
        result['department'] = department
        ans = json.dumps(result,ensure_ascii=False)
        f.write(ans)
        f.write('\n')
    f.close()
